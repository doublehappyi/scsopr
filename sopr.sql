-- MySQL Script generated by MySQL Workbench
-- 01/28/16 10:56:36
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema sopr
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema sopr
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sopr` DEFAULT CHARACTER SET utf8 ;
USE `sopr` ;

-- -----------------------------------------------------
-- Table `sopr`.`sopr_group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sopr`.`sopr_group` (
  `groupid` INT NOT NULL AUTO_INCREMENT,
  `groupname` VARCHAR(64) NOT NULL,
  `approver` VARCHAR(64) NULL,
  `createtime` DATETIME NOT NULL,
  `isdelete` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`groupid`),
  UNIQUE INDEX `groupName_UNIQUE` (`groupname` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sopr`.`sopr_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sopr`.`sopr_user` (
  `username` VARCHAR(64) NOT NULL,
  `groupid` INT NOT NULL,
  `createtime` DATETIME NOT NULL,
  `isdelete` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`username`),
  INDEX `fk_OPR_user_OPR_group1_idx` (`groupid` ASC),
  CONSTRAINT `fk_OPR_user_OPR_group1`
    FOREIGN KEY (`groupid`)
    REFERENCES `sopr`.`sopr_group` (`groupid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sopr`.`sopr_module`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sopr`.`sopr_module` (
  `moduleid` INT NOT NULL AUTO_INCREMENT,
  `modulekey` VARCHAR(64) NOT NULL,
  `modulename` VARCHAR(64) NOT NULL,
  `moduleurl` VARCHAR(128) NULL,
  `syncfile` VARCHAR(128) NULL,
  `createtime` DATETIME NOT NULL,
  `isdelete` INT NULL DEFAULT 0,
  PRIMARY KEY (`moduleid`),
  UNIQUE INDEX `moduleName_UNIQUE` (`modulekey` ASC),
  UNIQUE INDEX `moduleUrl_UNIQUE` (`moduleurl` ASC),
  UNIQUE INDEX `moduleTitle_UNIQUE` (`modulename` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sopr`.`sopr_group_module`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sopr`.`sopr_group_module` (
  `groupid` INT NOT NULL,
  `moduleid` INT NOT NULL,
  `permission` INT NOT NULL,
  INDEX `fk_OPR_group_module_OPR_group1_idx` (`groupid` ASC),
  INDEX `fk_OPR_group_module_OPR_module1_idx` (`moduleid` ASC),
  CONSTRAINT `fk_OPR_group_module_OPR_group1`
    FOREIGN KEY (`groupid`)
    REFERENCES `sopr`.`sopr_group` (`groupid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OPR_group_module_OPR_module1`
    FOREIGN KEY (`moduleid`)
    REFERENCES `sopr`.`sopr_module` (`moduleid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sopr`.`sopr_user_module`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sopr`.`sopr_user_module` (
  `username` VARCHAR(128) NOT NULL,
  `moduleid` INT NOT NULL,
  `permission` INT NULL,
  INDEX `fk_OPR_user_module_OPR_user1_idx` (`username` ASC),
  INDEX `fk_OPR_user_module_OPR_module1_idx` (`moduleid` ASC),
  CONSTRAINT `fk_OPR_user_module_OPR_user1`
    FOREIGN KEY (`username`)
    REFERENCES `sopr`.`sopr_user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_OPR_user_module_OPR_module1`
    FOREIGN KEY (`moduleid`)
    REFERENCES `sopr`.`sopr_module` (`moduleid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




insert into sopr_group values (1, "admin", "haohui", "2015-10-10 12:12:12", 0);
insert into sopr_group values (2, "guest", "guest", "2015-10-10 12:12:12", 0);
insert into sopr_group values (3, "search", "zengwenpeng", "2015-10-10 12:12:12", 0);


insert into sopr_user values("yishuangxi", 1, "2015-10-10 12:12:12", 0);
insert into sopr_user values("jiangfan10", 1, "2015-10-10 12:12:12", 0);
insert into sopr_user values("yina6", 1, "2015-10-10 12:12:12", 0);
insert into sopr_user values("haohui1", 1, "2015-10-10 12:12:12", 0);


insert into sopr_module values(1, "Index", "首页", "/Home/Index/index", "/Index.conf", "2015-10-10 12:12:12", 0);
insert into sopr_module values(2, "Group", "用户组管理", "/Admin/Group/index", "/Group.conf", "2015-10-10 12:12:12", 0);
insert into sopr_module values(3, "Module", "模块管理", "/Admin/Module/index", "/Module.conf", "2015-10-10 12:12:12", 0);
insert into sopr_module values(4, "User", "用户管理", "/Admin/User/index", "/User.conf", "2015-10-10 12:12:12", 0);

insert into sopr_group_module values (1, 1, 2),(1, 2, 2),(1, 3, 2),(1, 4, 2),
                                       (2, 1, 1),(2, 2, 0),(2, 3, 0),(2, 4, 0),
                                       (3, 1, 1),(3, 2, 1),(3, 3, 1),(3, 4, 1);