var app = angular.module('app', []);

app.controller('AppCtrl', ['$scope', '$http', function($scope, $http){
    $scope.toolbar = {
        groupName: "管理员",
        isDelete:1
    };
    $scope.rows = [
      { id: 1, groupName:"管理员1", isDelete:0, createTime:"2015-12-12 12:12:12" },
      { id: 2, groupName:"管理员2", isDelete:1, createTime:"2015-12-12 12:12:12" },
      { id: 3, groupName:"管理员3", isDelete:0, createTime:"2015-12-12 12:12:12" }
    ];
    
    $scope.pageInfo = {
        totalNum:200,
        currPage:2,
        totalPage:10,
        pageSize:20
    }
    
    $scope.loadPage = function(page){
        console.log(page);
    }

    $scope.showDetail = function(groupName){
        console.log(groupName);
        // alert(this.rows[index].groupName);
    };
}]);