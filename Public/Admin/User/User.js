/**
 * Created by yishuangxi on 2016/1/28.
 */
(function () {
    //全局数据容器:页面内容需要初始化，因为vue.js只会watch事先绑定的数据
    var vdata = {
        toolbar: {
            username: "",
            isdelete: 0
        },
        table: {
            list: [],
            totalNum: 0,
            totalPage: 0,
            page: 1,
            pageSize: 0
        },
        edit: {
            username:'',
            createtime:'',
            groupid:1,
            isdelete:'',
            modulelist:[],
            show:false

        },
        add: {
            username:'',
            groupid:1,//设置默认值，因为数据是异步拉取下来的，所以没切换就取不到
            isdelete:'',
            modulelist:[],
            show:false
        },
        grouplist:[]
    };

    var vmethods = {
        getItemList: getItemList,
        delItem: delItem,
        showEdit: showEdit,
        closeEdit:closeEdit,
        editItem:editItem,
        showAdd:showAdd,
        closeAdd:closeAdd,
        addItem:addItem

    };
    
    function resetGroupList(){
        var url = '/Admin/Group/getAllItemList';
        $.get(url).done(function(data){
            if(data && data.result){
                vdata.grouplist = data.data;
            }else{
                alert("获取组列表失败");
            }
        });
    }
    
    //获取列表接口
    function getItemList(username, isdelete, page) {
        var url = "/Admin/User/getItemList?username=" + username + "&isdelete=" + isdelete + "&page=" + page;
        $.get(url).done(function (data) {
            if (data && data.result) {
                vdata.table = data.data;
            }
        });
    }

    function delItem(username) {
        var url = "/Admin/User/delItem?username=" + username;
        var yes = confirm("确认删除？");
        if (yes) {
            $.get(url).done(function (data) {
                if (data && data.result) {
                    alert("删除成功！");
                } else {
                    alert("删除失败！"+data.errmsg);
                }
            });
        }
    }

    function showEdit(username, groupid, isdelete) {
        vdata.edit.username = username;
        vdata.edit.isdelete = isdelete;
        vdata.edit.groupid = groupid;
        resetEditModulelist(username);
        vdata.edit.show = true;
    }

    function closeEdit(){
        this.edit.show = false;
    }

    function editItem(){
        var moduleidPermsArr = [];
        var modulelist = vdata.edit.modulelist;
        for(var i = 0;  i < modulelist.length; i++){
            moduleidPermsArr.push(modulelist[i]['moduleid'] + ":" + modulelist[i]['permission']);
        }
        var moduleidPerms = moduleidPermsArr.join(',');

        var url = "/Admin/User/editItem?username=" + vdata.edit.username + "&groupid=" + vdata.edit.groupid + "&isdelete=" + vdata.edit.isdelete + "&moduleidPerms="+moduleidPerms;
        $.get(url).done(function (data) {
            if (data && data.result) {
                alert("编辑成功！");
            } else {
                alert("编辑失败！"+data.errmsg);
            }
        });
    }

    function showAdd() {
        vdata.add.show = true;
    }

    function closeAdd(){
        this.add.show = false;
    }

    function addItem(){
        var moduleidPermsArr = [];
        var modulelist = vdata.add.modulelist;
        for(var i = 0;  i < modulelist.length; i++){
            moduleidPermsArr.push(modulelist[i]['moduleid'] + ":" + modulelist[i]['permission']);
        }
        var moduleidPerms = moduleidPermsArr.join(',');

        var url = "/Admin/User/addItem?username=" + vdata.add.username + "&groupid="
            + vdata.add.groupid + "&isdelete=" + vdata.add.isdelete+"&moduleidPerms="+moduleidPerms;
        $.get(url).done(function (data) {
            if (data && data.result) {
                alert("添加成功！");
            } else {
                alert("添加失败！"+data.errmsg);
            }
        });
    }

    function resetAddModulelist(){
        var url = "/Admin/Module/getAllItemList";
        $.get(url).done(function(data){
            if(data && data.result){
                vdata.add.modulelist = data.data;
                for(var i = 0; i < vdata.add.modulelist.length; i++){
                    vdata.add.modulelist[i].permission = 0;
                }
            }else{
                alert("获取模块列表失败！"+data.errmsg);
            }
        });
    }

    function resetEditModulelist(username){
        var url = "/Admin/Module/getItemListByUsername?username="+username;
        $.get(url).done(function(data){
            if(data && data.result){
                vdata.edit.modulelist = data.data;
            }else{
                alert("获取模块列表失败！"+data.errmsg);
            }
        });
    }

    //vm对象
    var vm = new Vue({
        el: '#page',
        data: vdata,
        methods: vmethods,
        computed:{
            editUsernameList:function(){
                var usernameList = [];
                for(var i = 0; i <this.edit.userlist.length; i++){
                    usernameList.push(this.edit.userlist[i].username);
                }
                return usernameList.join(";");
            }
        }
    });

    getItemList(vdata.toolbar.username, vdata.toolbar.isdelete, vdata.table.page);
    resetGroupList();
    resetAddModulelist();
    window.vm = vm;
    window.vdata = vdata;
})();