/**
 * Created by yishuangxi on 2016/1/28.
 */
(function () {
    //全局数据容器:页面内容需要初始化，因为vue.js只会watch事先绑定的数据
    var vdata = {
        toolbar: {
            groupname: "",
            isdelete: 0
        },
        table: {
            list: [],
            totalNum: 0,
            totalPage: 0,
            page: 1,
            pageSize: 0
        },
        edit: {
            groupid:'',
            groupname:'',
            approver:'',
            createtime:'',
            isdelete:'',
            userlist:[],
            modulelist:[],
            usernames:"",
            show:false

        },
        add: {
            groupname:'',
            approver:'',
            isdelete:0,
            modulelist:[],
            show:false
        },
        detail:{
            groupid:'',
            groupname:'',
            approver:'',
            createtime:'',
            isdelete:'',
            userlist:[],
            modulelist:[],
            show:false
        },
        init:{
            modulelist:[]
        }
    };

    var vmethods = {
        getItemList: getItemList,
        delItem: delItem,
        showEdit: showEdit,
        closeEdit:closeEdit,
        editItem:editItem,
        showAdd:showAdd,
        closeAdd:closeAdd,
        addItem:addItem,
        showDetail:showDetail

    };

    //获取列表接口
    function getItemList(groupname, isdelete, page) {
        var url = "/Admin/Group/getItemList?groupname=" + groupname + "&isdelete=" + isdelete + "&page=" + page;
        $.get(url).done(function (data) {
            if (data && data.result) {
                vdata.table = data.data;
            }
        });
    }

    function delItem(groupid) {
        var url = "/Admin/Group/delItem?groupid=" + groupid;
        var yes = confirm("确认删除？");
        if (yes) {
            $.get(url).done(function (data) {
                if (data && data.result) {
                    alert("删除成功！");
                } else {
                    alert("删除失败！"+data.errmsg);
                }
            });
        }
    }

    function showEdit(groupid, groupname, approver, createtime, isdelete) {
        vdata.edit.groupid = groupid;
        vdata.edit.groupname = groupname;
        vdata.edit.approver = approver;
        vdata.edit.createtime = createtime;
        vdata.edit.isdelete = isdelete;
        resetEditUserlist(groupid);
        resetEditModulelist(groupid);
        vdata.edit.show = true;
    }

    function closeEdit(){
        this.edit.show = false;
    }

    function showAdd() {
        vdata.add.show = true;
        resetAddModulelist();
    }

    function closeAdd(){
        this.add.show = false;
    }

    function addItem(){
        var moduleidPermsArr = [];
        var modulelist = vdata.add.modulelist;
        for(var i = 0;  i < modulelist.length; i++){
            moduleidPermsArr.push(modulelist[i]['moduleid'] + ":" + modulelist[i]['permission']);
        }
        var moduleidPerms = moduleidPermsArr.join(',');
        var usernames = $("#add").find('textarea').val();

        var url = "/Admin/Group/addItem?groupname=" + vdata.add.groupname + "&approver=" + vdata.add.approver + "&isdelete=" + vdata.add.isdelete +
            "&moduleidPerms="+moduleidPerms+"&usernames="+usernames;
        $.get(url).done(function (data) {
            if (data && data.result) {
                if(data.data.existUsernames){
                    alert("添加成功！但以下用户已经有分组"+data.data.existUsernames);
                }else{
                    alert("添加成功！");
                }
            } else {
                alert("添加失败！"+data.errmsg);
            }
        });
    }

    function showDetail(groupid, groupname, approver, createtime, isdelete) {
        vdata.detail.groupid = groupid;
        vdata.detail.groupname = groupname;
        vdata.detail.approver = approver;
        vdata.detail.createtime = createtime;
        vdata.detail.isdelete = isdelete;
        vdata.detail.show = true;
    }

    function resetEditUserlist(groupid){
        var url = "/Admin/User/getItemListByGroupId?groupid="+groupid;
        $.get(url).done(function(data){
            if(data && data.result){
                vdata.edit.userlist = data.data;
            }else{
                alert("获取用户列表失败！"+data.errmsg);
            }
        });
    }

    function resetEditModulelist(groupid){
        var url = "/Admin/Module/getItemListByGroupId?groupid="+groupid;
        $.get(url).done(function(data){
            if(data && data.result){
                vdata.edit.modulelist = data.data;
            }else{
                alert("获取模块列表失败！"+data.errmsg);
            }
        });
    }

    function resetAddModulelist(){
        var url = "/Admin/Module/getAllItemList";
        $.get(url).done(function(data){
            if(data && data.result){
                vdata.add.modulelist = data.data;
            }else{
                alert("获取模块列表失败！"+data.errmsg);
            }
        });
    }


    function editItem(){
        var moduleidPermsArr = [];
        var modulelist = vdata.edit.modulelist;
        for(var i = 0;  i < modulelist.length; i++){
            moduleidPermsArr.push(modulelist[i]['moduleid'] + ":" + modulelist[i]['permission']);
        }
        var moduleidPerms = moduleidPermsArr.join(',');
        var usernames = $("#edit").find('textarea').val();
        var url = "/Admin/Group/editItem?groupid="+vdata.edit.groupid+"&groupname="+vdata.edit.groupname+
            "&approver="+vdata.edit.approver+"&isdelete="+vdata.edit.isdelete+"&moduleidPerms="+moduleidPerms+"&usernames="+usernames;
        $.get(url).done(function(data){
            if(data && data.result){
                if(data.data.existUsernames){
                    alert("编辑成功！但以下用户已有分组："+data.data.existUsernames);
                }else{
                    alert("编辑成功！");
                }
            }else{
                alert("编辑失败"+data.errmsg);
            }
        });
    }

    
    //获取所有的modulelist并渲染到编辑页和新增页面去
    //function renderAllModuleList(){
    //    var url = "/Admin/Module/getAllItemList";
    //    $.get(url).done(function(data){
    //        if(data && data.result){
    //            vdata.init.modulelist = data.data;
    //            for(var i = 0; i < vdata.init.modulelist.length; i++){
    //                vdata.init.modulelist[i].permission = 0;
    //            }
    //        }else{
    //            alert("获取模块列表失败！"+data.errmsg);
    //        }
    //    });
    //}
    
    //vm对象
    var vm = new Vue({
        el: '#page',
        data: vdata,
        methods: vmethods,
        computed:{
            editUsernameList:function(){
                var usernameList = [];
                for(var i = 0; i <this.edit.userlist.length; i++){
                    usernameList.push(this.edit.userlist[i].username);
                }
                return usernameList.join(",");
            }
        }
    });

    getItemList(vdata.toolbar.groupname, vdata.toolbar.isdelete, vdata.table.page);
    //resetAddModulelist();
    //renderAllModuleList();
    window.vm = vm;
    window.vdata = vdata;
})();