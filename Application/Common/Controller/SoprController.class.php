<?php
namespace Common\Controller;

use Think\Controller;
use Think\Model;

class SoprController extends Controller
{
    const LOGTYPE_LOGIN = "登录";
    const LOGTYPE_LOGOUT = "退出";
    const LOGTYPE_NEW = "新增";
    const LOGTYPE_DELETE = "删除";
    const LOGTYPE_MODIFY = "修改";
    const LOGTYPE_PUB = "发布";
    const LOGTYPE_EXPORT = "导出";
    const LOGTYPE_IMPORT = "导入";
    const LOGTYPE_REFRESH = "刷新";

    const DEFAULT_PAGESIZE = 5;

    //权限常量
    const PERM_ZERO = 0;
    const PERM_READ = 1;
    const PERM_WRITE = 2;
    const PERM_PUBLISH = 3;

    const MAXED_PAGESIZE = 10000;

    //登录校验配置
    const ERP_LOGIN_DOMAIN = 'erp1.jd.com';
//    const ERP_LOGIN_URL = 'http://erp1.360buy.com/newHrm/Verify.aspx?ReturnUrl='; //即将过期
//    const ERP_LOGIN_KEY = '8B6697227CBCA902B1A0925D40FAA00B353F2DF4359D2099'; //测试环境的key
    const ERP_LOGIN_URL = 'http://ssa.jd.com/sso/login?ReturnUrl=';//已经开始使用
    const ERP_LOGIN_KEY = 'C602924B0D1090D931E3771D74ABBF9733A8C3545CFE1810'; //线上环境的key

    protected $modulekey = "";
    private $soprLogDao = null;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *访问主页鉴权
     **/
//    protected function index()
//    {
//        $this->checkLogin();
//		if(!$this->checkReadPerm()){
//			redirect("/Home/UnAuthorized/index");
//		}
//    }
//
//    /**
//     *访问列表数据鉴权
//     **/
//    public function getItemList()
//    {
//        $this->checkLogin();
//        if (!$this->checkReadPerm()) {
//            $this->ajaxReturnError("您没有读取权限");
//        }
//    }
//
//    /**
//     *访问列表详情鉴权
//     **/
//    public function getItemDetail()
//    {
//        $this->checkLogin();
//        if (!$this->checkReadPerm()) {
//            $this->ajaxReturnError("您没有读取权限");
//        }
//    }
//
//    /**
//     *添加数据鉴权
//     **/
//    public function addItem()
//    {
//        $this->checkLogin();
//        if (!$this->checkWritePerm()) {
//            $this->ajaxReturnError("您没有操作权限");
//        }
//    }
//
//    /**
//     *编辑数据鉴权
//     **/
//    public function editItem()
//    {
//        $this->checkLogin();
//        if (!$this->checkWritePerm()) {
//            $this->ajaxReturnError("您没有操作权限");
//        }
//    }
//
//    /**
//     *删除数据鉴权
//     **/
//    public function delItem()
//    {
//        $this->checkLogin();
//        if (!$this->checkWritePerm()) {
//            $this->ajaxReturnError("您没有操作权限");
//        }
//    }

    /**
     * Ajax Json 返回错误信息
     * @param unknown $errmsg
     */
    protected function ajaxReturnError($errmsg)
    {
        $ret = array();
        $ret ["result"] = false;
        $ret ["errmsg"] = $errmsg;
        $this->ajaxReturn($ret, "JSON");
        return;
    }


    /**
     * Ajax Json 返回成功信息
     * @param string $data 数据
     * @param string $pageinfo 数据分页
     */
    protected function ajaxReturnSuccess($data = null, $pageinfo = null)
    {
        $ret = array();
        $ret ["result"] = true;
        $ret ["data"] = $data;
        $ret["pageinfo"] = $pageinfo;
        $this->ajaxReturn($ret, "JSON");
        return;
    }

    /**
     * 从Session获取当前登录用户信息
     * @param string $fieldName
     * @return mixed
     */
    protected function getUserFromSession($fieldName = "")
    {
        $userInfo = session("CUR_LOGIN_USER");
        if (is_null($userInfo)) {
            $this->redirect("/Home/Index/index", null, 0, "");
        } else {
            if (isValidString($fieldName) && array_key_exists($fieldName, $userInfo)) {
                return $userInfo[$fieldName];
            } else {
                return $userInfo;
            }
        }
    }

    /**
     * 更新当前登录用户信息
     * @param unknown $userInfo
     */
    protected function setUserIntoSession($userInfo)
    {
        session("CUR_LOGIN_USER", $userInfo);
    }

    /**
     * 校验用户页面操作权限
     * @return boolean
     */
// 	protected function checkUserRight(){
// 		$modulekey=$this->modulekey;
// 		$userName=$this->getUserFromSession ( "userName");
// 		if(isValidString($userName) && isValidString($modulekey)){
// 			$dao=new \Think\Model();
// 			$sql="select 1 from sopr_module where modulekey='%s' and moduleid in
// 					(select moduleid from sopr_groupmodule where groupid=
// 					(select groupid from sopr_user where userName='%s'))";
// 			$sql=sprintf($sql,$modulekey,$userName);
// 			$list=$dao->query ( $sql );
// 			if(count($list)>0){
// 				return true;
// 			}
// 		}
// 		return false;
// 	}
    /**
     * 获取用户页面操作权限私有方法
     * @return int
     */
    private function getUserPerm()
    {
        $modulekey = $this::MODULE_KEY;
        $username = $this->getUsernameFromCookie();
        if (isValidString($modulekey) && isValidString($username)) {
            $dao = new \Think\Model();

            $sql = " select
                        max(permission) as permission
                     from
                        (select
                            max(permission) as permission
                        from sopr_group_module
                            where moduleid=(select moduleid from sopr_module where modulekey='%s') and groupid=(select groupid from sopr_user where username='%s')
                        union
                        select
                            max(permission) as permission
                        from sopr_user_module
                            where moduleid=(select moduleid from sopr_module where modulekey='%s') and username='%s') as perms";
            $sql = sprintf($sql, $modulekey, $username, $modulekey, $username);

//            $sql = "select permission from sopr_group_module where
//            moduleid=(select moduleid from sopr_module where modulekey='%s')
//            and
//            groupid=(select groupid from sopr_user where username='%s')";
//            $sql = sprintf($sql, $modulekey, $username);

            $rows = $dao->query($sql);

            try {
                if ($rows && count($rows) > 0) {
                    return intval($rows[0]["permission"]);
                }
//                else{
//                    $sql = "select permission from sopr_user_module where
//                        moduleid=(select moduleid from sopr_module where modulekey='%s')
//                        and
//                        username='%s'";
//                    $sql = sprintf($sql, $modulekey, $username);
//                    $rows = $dao->query($sql);
//                    if ($rows && count($rows) > 0) {
//                        return intval($rows[0]["permission"]);
//                    }
//                }

            } catch (\Exception $e) {
                return false;
            }
        }else{
            return 0;
        }
    }

    protected function checkLogin()
    {
        $ERP_LOGIN_COOKIE_NAME = C('ERP_LOGIN_COOKIE_NAME');
        $ERP_LOGIN_URL = C('ERP_LOGIN_URL');
        $erpLoginCookie = cookie($ERP_LOGIN_COOKIE_NAME);
        if ($erpLoginCookie) {
            return true;
        } else {
            $erpLoginUrl = $ERP_LOGIN_URL . urlencode($this->getCurrUrl());
            redirect($erpLoginUrl);
        }
    }

    protected function getUsernameFromCookie()
    {
        $client = new \Home\Common\JdUser ();
        $client->init_user(cookie(C('ERP_LOGIN_COOKIE_NAME')), C('ERP_LOGIN_KEY'));
        $username = iconv("GB2312", "UTF-8", $client->username);
        return $username;
    }

    protected function checkReadPerm($isJson=true)
    {
        $this->checkLogin();
        $perm = $this->getUserPerm();
        if ($perm >= $this::PERM_READ) {
            return true;
        } else {
            if($isJson){
                $this->ajaxReturnError("您没有读取权限");
            }else{
                redirect("/Home/UnAuthorized/index");
            }
        }
    }

    protected function checkWritePerm($isJson=true)
    {
        $this->checkLogin();
        $perm = $this->getUserPerm();
        if ($perm >= $this::PERM_WRITE) {
            return true;
        } else {
            if($isJson){
                $this->ajaxReturnError("您没有写取权限");
            }else{
                redirect("/Home/UnAuthorized/index");
            }
        }
    }

    protected function checkPublishPerm()
    {
        $perm = $this->getUserPerm();
        if ($perm >= $this::PERM_PUBLISH) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取当前页面模块信息（模块对应权限）
     * @return \Think\mixed|boolean
     */
//    protected function getModuleInfo()
//    {
//        $dao = new \Manage\Model\ModuleModel();
//        $list = $dao->getModule(-1, $this->modulekey, "", 0, false);
//        if (count($list) > 0)
//            return $list[0];
//        return false;
//    }

    /**
     * 写日志
     * @param unknown $operate
     * @param unknown $keyword
     * @param unknown $content
     * @param string $platform
     * @return \Think\false
     */
    protected function writeLog($operate, $keyword, $content, $platform = "jddj")
    {
        $userName = $this->getUserFromSession("userName");
        if ($this->soprLogDao == null) {
            $this->soprLogDao = new \Admin\Model\LogModel();
        }
        return $this->soprLogDao->addLog($userName, $platform, $this->modulekey, $operate, $keyword, $content);
    }

    /**
     * 获取登录IP
     * @return Ambigous <string, unknown>
     */
    protected function getClientIP()
    {
        $IPaddress = '';
        if (isset ($_SERVER)) {
            if (isset ($_SERVER ["HTTP_X_FORWARDED_FOR"])) {
                $IPaddress = $_SERVER ["HTTP_X_FORWARDED_FOR"];
            } else if (isset ($_SERVER ["HTTP_CLIENT_IP"])) {
                $IPaddress = $_SERVER ["HTTP_CLIENT_IP"];
            } else {
                $IPaddress = $_SERVER ["REMOTE_ADDR"];
            }
        } else {
            if (getenv("HTTP_X_FORWARDED_FOR")) {
                $IPaddress = getenv("HTTP_X_FORWARDED_FOR");
            } else if (getenv("HTTP_CLIENT_IP")) {
                $IPaddress = getenv("HTTP_CLIENT_IP");
            } else {
                $IPaddress = getenv("REMOTE_ADDR");
            }
        }
        return $IPaddress;
    }

    protected function getCurrUrl()
    {
        $sys_protocal = isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
        $php_self = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
        $path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
        $relate_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $php_self . (isset($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING'] : $path_info);
        return $sys_protocal . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '') . $relate_url;
    }

}