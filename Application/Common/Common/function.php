<?php
/**
*获取当前时间
 **/
function getCurrentDatetime(){
	date_default_timezone_set("Asia/Shanghai");
	return date("Y-m-d H:i:s");
}

/**
 * 是否是删除值:为0或者1
 */
function isDeleteInt($var){
	return is_int($var) && ($var === 1 || $var === 0);
}

/**
 * 是否是权限值:为0或者1
 */
function isPermInt($var){
	return is_int($var) && ($var === 0 || $var === 1 || $var === 2);
}

/**
 * 是否是正整数值：1,2,3,...
 */
function isPositiveInt($var){
	return is_int($var) && $var > 0;
}

/**
 * 是否是字符串：1,2,3,...
 */
function isString($var){
	return is_string($var);
}

/**
 * 是否是非空字符串
 */
function isNoEmptyString($var){
	return is_string($var) && !empty($var);
}

/**
 * 是否非空字符串,相当于isNullOrEmpty()取反
 */
function isValidString($var) {
	if (is_null ( $var )) {
		return false;
	} else if (is_string ( $var )) {
		return !empty ( $var );
	} else {
		return false;
	}
}

/**
 * 是否空字符串,相当于isNullOrEmpty()
 */
function isNoValidString($var){
	return !isValidString($var);
}

/**
 * 是否有效商品ID
 */
function isValidGoodsId($goodsid){
	if(is_string($goodsid) && strlen($goodsid)==32){
		for($i=0;$i<32;$i++){
			if(!(($goodsid[$i]>="A" && $goodsid[$i]<="F") || ($goodsid[$i]>="0" && $goodsid[$i]<="9"))){
				return false;
			}
		}
		$tempstr=substr($goodsid,6,2).substr($goodsid,4,2).substr($goodsid,2,2).substr($goodsid,0,2);
		$index=hexdec($tempstr);//QQ号
		if($index<10000){
			return false;
		}
		return true;
	}else{
		return false;
	}
}

/**
 * 是否YYYY-MM-DD格式的日期
 */
function isDateString($var){
	if(!preg_match("/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$/",$var))
	{
		return false;
	}
	$temp=$var." 23:59:59";
	return isDateTimeString($temp);
}

/**
 * 是否YYYY-MM-DD HH:mm:ss格式的日期时间
 */
function isDateTimeString($var){
	if(!preg_match("/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})$/",$var,$arr))
	{
		return false;
	}
	$temp=strtotime($var);
	if($temp===-1 || $temp===false){
		return false;
	}
	return true;
}

/**
 * 是否HH:mm:ss格式的日期时间
 */
function isTimeString($var){
	if(!preg_match("([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",$var))
	{
		return false;
	}
	$temp="1970-01-01 ".$var;
	return isDateTimeString($temp);
}

/**
 * 是否非负整数
 */
function isPositiveNumeric($var){
	if(is_null($var)){
		return false;
	}else if(is_numeric($var)){
		$temp=intval($var);
		return $temp>=0;
	}else{
		return false;
	}
}

/**
 * 是否负整数
 */
function isNegativeNumeric($var){
	if(is_null($var)){
		return false;
	}else if(is_numeric($var)){
		$temp=intval($var);
		return $temp<0;
	}else{
		return false;
	}
}

function createTencentCdnImgName()
{
	mt_srand((double)microtime()*10000);
	$timehex=strtoupper(dechex(time()));
	$uuid=strtoupper(md5(uniqid(rand(), true)));
	$str=sprintf("pcfx-%s-%s.0",$timehex,$uuid);
	return $str;
}

/**
 * 转换编码
 * @param unknown $incode
 * @param unknown $outcode
 * @param unknown $var
 * @return unknown|string
 */
function  ConvertEncode($incode,$outcode,$var){
	if($incode==str_replace("//IGNORE","",$outcode)){
		//编码相同
		return $var;
	}
	if(is_string($var)){
		return iconv($incode, $outcode, $var);
	}
	else if(is_array($var)){
		foreach($var as $key=>$value){
			$var[$key]=ConvertEncode($incode,$outcode,$value);
		}
		return $var;
	}
	else if(is_object($var)){
		$reflectionClass=new ReflectionClass(get_class($var));
		$properties = $reflectionClass->getProperties();
		foreach($properties as $property) {
			$name=$property->getName();
			$property->setAccessible(true);
			$value=ConvertEncode($incode,$outcode,$property->getValue($var));
			$property->setValue($var,$value);
		}
	}
	return $var;
}

function GBK2UTF8($var){
	return ConvertEncode("GBK","UTF-8//IGNORE",$var);
}

function UTF82GBK($var){
	return ConvertEncode("UTF-8","GBK//IGNORE",$var);
}