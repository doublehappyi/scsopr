<?php

namespace Common\Model;

use Think\Model;

class SoprModel extends Model {
    const PAGE_SIZE=2;
    public function __construct($name='',$tablePrefix='',$connection=''){
        parent::__construct($name,$tablePrefix,$connection);
    }

    public function query($sql,$parse=false){
        return parent::query($sql,$parse);
    }

    public function execute($sql,$parse=false) {
        return parent::execute($sql,$parse);
    }
}
