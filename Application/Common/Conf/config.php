<?php
return array(
	//'配置项'=>'配置值'
	'URL_MODEL' => '2',
	//数据库配置项
	'DB_TYPE'      =>  'mysql',     // 数据库类型
    'DB_HOST'      =>  '127.0.0.1',     // 服务器地址
    'DB_NAME'      =>  'sopr',     // 数据库名
    'DB_USER'      =>  'root',     // 用户名
    'DB_PWD'       =>  '',     // 密码
    'DB_PORT'      =>  '3306',     // 端口
    'DB_PREFIX'    =>  '',     // 数据库表前缀
    // 'DB_DSN'       =>  '',     // 数据库连接DSN 用于PDO方式
    'DB_CHARSET'   =>  'utf8', // 数据库的编码 默认为utf8
    'DB_DEBUG'  =>  TRUE, // 数据库调试模式 开启后可以记录SQL日志
	
	//erp登录接入相关配置：通过ERP_LOGIN_COOKIE_NAME获取cookie值，通过ERP_LOGIN_KEY解密出用户名
	//通过ERP_LOGIN_URL重定向页面
	//另外登录接入先关php文件在\Home\Common下JdCryptAES.class.php, JdUser.class.php
	'ERP_LOGIN_COOKIE_NAME'=>'erp1.jd.com',//erp登录的cookie名
//	'ERP_LOGIN_URL'=>'http://ssa.jd.com/sso/login?ReturnUrl=',//erp登录地址
	'ERP_LOGIN_URL'=>'http://erp1.jd.com/newHrm/Verify.aspx?ReturnUrl=',//erp登录地址
	'ERP_LOGIN_KEY'=>'C602924B0D1090D931E3771D74ABBF9733A8C3545CFE1810',//erp登录cookie解密key
);