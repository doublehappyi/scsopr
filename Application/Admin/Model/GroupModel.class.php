<?php
namespace Admin\Model;

use \Common\Model\SoprModel;

class GroupModel extends SoprModel
{
    protected $trueTableName = 'sopr_group';

    /**
     *新增条目
     **/
    public function addItem($groupname, $approver, $isdelete)
    {
        $createtime = getCurrentDatetime();
        $sql = "insert into sopr_group values (null, '%s', '%s', '%s', %s)";
        $sql = sprintf($sql, $groupname, $approver, $createtime, $isdelete);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function addItemTrans($groupname, $approver, $isdelete, $moduleArr, $usernameArr)
    {
        //1,插入组表
        //2，插入组权限表:先获取刚刚插入的表ID
        //3，插入用户表：先判断用户是否已经存在，存在则记录进$existUsernames,否则插入，并设置groupid为当前groupid，isDelete=0；
        $createtime = getCurrentDatetime();

        try {
            $this->startTrans();
            //1,
            $ret = $this->execute(sprintf("insert into sopr_group values (null, '%s', '%s', '%s', %s)", $groupname, $approver, $createtime, $isdelete));
            if ($ret === false) {
                $this->rollback();
                return false;
            }
            //2,
            $groupid = $this->query(sprintf("select groupid from sopr_group where groupname='%s'", $groupname));
            if ($groupid === false) {
                $this->rollback();
                return false;
            }
            $groupid = $groupid[0]['groupid'];
            $sql = "insert into sopr_group_module values ";
            $values = array();
            for ($i = 0, $len = count($moduleArr); $i < $len; $i++) {
                array_push($values, sprintf("(%s, %s, %s)", $groupid, $moduleArr[$i][0], $moduleArr[$i][1]));
            }
            $sql = $sql . join(",", $values);
            if ($this->execute($sql) === false) {
                $this->rollback();
                return false;
            }

            //3,
            $existUsernames = "";
            for ($i = 0, $len = count($usernameArr); $i < $len; $i++) {
                $currUsername = $usernameArr[$i];
                $ret = $this->query(sprintf("select 1 from sopr_user where username='%s'", $currUsername));
                if ($ret === false) {
                    $this->rollback();
                    return false;
                } elseif (count($ret) > 0) {
                    $existUsernames = $existUsernames . ",".$currUsername;
                } else if (count($ret) === 0) {
                    $ret = $this->execute(sprintf("insert into sopr_user values('%s', %d, '%s', 0)", $currUsername, $groupid, $createtime));
                    if($ret=== false){
                        $this->rollback();
                        return false;
                    }
                }
            }

            $this->commit();
            return array("existUsernames"=>$existUsernames);
        } catch (\Exception $e) {
            $this->rollback();
            return false;
        }

    }

    /**
     *获取列表
     **/
    public function getItemList($groupname, $isdelete, $page)
    {
        $sql = "select groupid, groupname, approver, createtime, isdelete
        from sopr_group
        where groupname like '%%%s%%' and isdelete=%s limit %s, %s";
        $sql = sprintf($sql, $groupname, $isdelete, ($page - 1) * $this::PAGE_SIZE, $this::PAGE_SIZE);

        $totalnumSql = "select count(groupid) as totalnum
        from sopr_group
        where groupname like '%%%s%%' and isdelete=%s";
        $totalnumSql = sprintf($totalnumSql, $groupname, $isdelete);

        try {
            $list = $this->query($sql);
            $totalnum = $this->query($totalnumSql);
            $totalnum = $totalnum[0]["totalnum"];
            $totalpage = round($totalnum / $this::PAGE_SIZE);
            return array("list" => $list, "totalnum" => intval($totalnum[0]), "page" => $page, "totalpage" => $totalpage, pagesize => $this::PAGE_SIZE);
        } catch (\Exception $e) {
            return false;
        }
    }

    /***
     * 获取列表所有数据
     **/
    public function getAllItemList()
    {
        $sql = "select groupid, groupname, approver, createtime, isdelete from sopr_group";

        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取详情
     **/
    public function getItemDetail($groupid)
    {
        $sql = "select groupid, groupname, approver, createtime, isdelete from sopr_group where groupid=%s";
        $sql = sprintf($sql, $groupid);

        try {
            $list = $this->query($sql);
            return $list[0];
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *更新条目
     **/
    public function editItem($groupid, $groupname, $approver, $isdelete)
    {
        $sql = "update sopr_group set groupname='%s', approver='%s', isdelete=%s where groupid=%s";
        $sql = sprintf($sql, $groupname, $approver, $isdelete, $groupid);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }


    /**
     *更新条目:带权限
     **/
    public function editItemTrans($groupid, $groupname, $approver, $isdelete, $moduleArr, $usernameArr)
    {

        //1,更新组表
        //2，删除旧权限
        //3，插入新权限表
        //4，判断用户用户是否已经存在？存在则再判断其groupid是否和当前groupid一致，不一致需要添加至$existUsernames，一致则略过，
        //4,当前用户不存在，则新建用户
        //0，把不在当前usernamArr里面的用户移除到游客组
        $createtime = getCurrentDatetime();

        try {
            $this->startTrans();
            //0,
            $ret = $this->query(sprintf("select username from sopr_user where groupid=%s", $groupid));
            if($ret === false){
                $this->rollback();
                return false;
            }
            for($i = 0, $len=count($ret); $i<$len; $i++){
                if(!in_array($ret[$i]["username"], $usernameArr)){
                    if($this->execute(sprintf("update sopr_user set groupid=2 where username='%s'", $ret[$i]["username"])) === false){
                        $this->rollback();
                        return false;
                    }
                }
            }

            //1,
            $ret = $this->execute(sprintf("update sopr_group set groupname='%s', approver='%s', isdelete=%s where groupid=%s", $groupname, $approver, $isdelete, $groupid));
            if ($ret === false) {
                $this->rollback();
                return false;
            }
            //2
            $ret = $this->execute(sprintf("delete from sopr_group_module where groupid='%s'",$groupid));
            if ($ret === false) {
                $this->rollback();
                return false;
            }
            //3
            $sql = "insert into sopr_group_module values ";
            $values = array();
            for ($i = 0, $len = count($moduleArr); $i < $len; $i++) {
                array_push($values, sprintf("(%s, %s, %s)", $groupid, $moduleArr[$i][0], $moduleArr[$i][1]));
            }
            $sql = $sql . join(",", $values);
            if ($this->execute($sql) === false) {
                $this->rollback();
                return false;
            }

            //4,
            $existUsernames = "";
            for ($i = 0, $len = count($usernameArr); $i < $len; $i++) {
                $currUsername = $usernameArr[$i];
                $sql = sprintf("select groupid from sopr_user where username='%s'", $currUsername);
                $ret = $this->query($sql);
//                dump($ret);
                if ($ret === false) {
                    $this->rollback();
                    return false;
                } elseif (count($ret) > 0) {
                    if($ret[0]["groupid"] != $groupid){
                        $existUsernames = $existUsernames .$currUsername. ",";
                    }
                } else if (count($ret) === 0) {
                    $ret = $this->execute(sprintf("insert into sopr_user values('%s', %d, '%s', 0)", $currUsername, $groupid, $createtime));
                    if($ret=== false){
                        $this->rollback();
                        return false;
                    }
                }
            }


            $this->commit();
            return array("existUsernames"=>$existUsernames);
        } catch (\Exception $e) {
            $this->rollback();
            return false;
        }
//        //更新group表语句
//        $groupSql = sprintf("update sopr_group set groupname='%s', approver='%s', isdelete=%s where groupid=%s", $groupname, $approver, $isdelete, $groupid);
//        //删除group_module中的旧权限数据
//        $delGroupModuleSql = sprintf("delete from sopr_group_module where groupid=%s", $groupid);
//        //增加group_module新权限数据
//        $insertGroupModuleSql = "insert into sopr_group_module values ";
//        $values = array();
//        for ($i = 0, $len = count($moduleArr); $i < $len; $i++) {
//            array_push($values, sprintf("(%s, %s, %s)", $groupid, $moduleArr[$i][0], $moduleArr[$i][1]));
//        }
//        $insertGroupModuleSql = $insertGroupModuleSql . join(",", $values);
//
//
//
//        try {
//            $this->startTrans();
//            //1，先更新sopr_group表数据
//            if ($this->execute($groupSql) === false) {
//                $this->rollback();
//                return false;
//            }
//            //2，全删sopr_group_module旧数据
//            if ($this->execute($delGroupModuleSql) === false) {
//                $this->rollback();
//                return false;
//            }
//            //3，插入全部新权限数据
//            if ($this->execute($insertGroupModuleSql) === false) {
//                $this->rollback();
//                return false;
//            }
//
//            $this->commit();
//            return true;
//        } catch (\Exception $e) {
//            $this->rollback();
//            return false;
//        }

    }

    /**
     *删除条目
     **/
    public function delItem($groupid)
    {
        $sql = "update sopr_group set isdelete=1 where groupid=%s";
        $sql = sprintf($sql, $groupid);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *删判断groupname是否已经存在
     **/
    public function isItemExist($groupname)
    {
        $sql = "select 1 from sopr_group where groupname='%s'";
        $sql = sprintf($sql, $groupname);
        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }
}