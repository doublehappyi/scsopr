<?php
namespace Admin\Model;

use \Common\Model\SoprModel;

class ModuleModel extends SoprModel
{
    protected $trueTableName = 'sopr_module';

    /**
     *新增条目
     **/
    public function addItem($modulekey, $modulename, $moduleurl, $syncfile, $isdelete)
    {
        $createtime = getCurrentDatetime();
        $sql = "insert into sopr_module values (null, '%s', '%s', '%s', '%s', '%s', %s)";
        $sql = sprintf($sql, $modulekey, $modulename, $moduleurl, $syncfile, $createtime, $isdelete);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取列表
     **/
    public function getItemList($modulekey, $modulename, $isdelete, $page)
    {
        $sql = "select moduleid, modulekey, modulename, moduleurl, syncfile, createtime, isdelete
        from sopr_module
        where modulekey like '%%%s%%' and modulename like '%%%s%%' and  isdelete=%s limit %s, %s";
        $sql = sprintf($sql, $modulekey, $modulename, $isdelete, ($page - 1) * $this::PAGE_SIZE, $this::PAGE_SIZE);

        $totalnumSql = "select count(moduleid) as totalnum
        from sopr_module
        where modulekey like '%%%s%%' and modulename like '%%%s%%' and  isdelete=%s";
        $totalnumSql = sprintf($totalnumSql, $modulekey, $modulename, $isdelete);

        try {
            $list = $this->query($sql);
            $totalnum = $this->query($totalnumSql);
            $totalnum = $totalnum[0]["totalnum"];
            $totalpage = round($totalnum/$this::PAGE_SIZE);
            return array("list"=>$list, "totalnum"=>intval($totalnum[0]), "page"=>$page, "totalpage"=>$totalpage, pagesize=>$this::PAGE_SIZE);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取列表:grouid
     **/
    public function getItemListByGroupId($groupid)
    {
//        $sql = "select groupid , moduleid, permission,
//          (select modulename from sopr_module where moduleid=sopr_group_module.moduleid ) as modulename
//          from sopr_group_module where groupid=%s";
        //
        $sql = "select moduleid, modulename, 0 as permission from sopr_module where moduleid not in (select moduleid from sopr_group_module where groupid=%s)
          union
          select moduleid, (select modulename from sopr_module where moduleid=sopr_group_module.moduleid) as modulename, permission from sopr_group_module where groupid=%s";
        $sql = sprintf($sql, $groupid, $groupid);


        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取列表:username
     **/
    public function getItemListByUsername($username)
    {
//        $sql = "select username , moduleid, permission, (select modulename from sopr_module where moduleid=sopr_user_module.moduleid ) as modulename from sopr_user_module where username='%s'";
        $sql = "select moduleid, modulename, 0 as permission from sopr_module where moduleid not in (select moduleid from sopr_user_module where username='%s')
                union
                select moduleid,  (select modulename from sopr_module where moduleid=sopr_user_module.moduleid) as modulename, permission from sopr_user_module where username='%s'";
        $sql = sprintf($sql, $username, $username);


        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取列表
     **/
    public function getAllItemList()
    {
//        $sql =  "select moduleid, modulekey, modulename, moduleurl, syncfile, createtime, isdelete from sopr_module";
        $sql =  "select moduleid, modulename, 0 as permission from sopr_module where isdelete!=1";
        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }


    /**
     *获取详情
     **/
    public function getItemDetail($moduleid)
    {
        $sql = "select moduleid, modulekey, modulename, moduleurl, syncfile, createtime, isdelete from sopr_module where moduleid=%s";
        $sql = sprintf($sql, $moduleid);

        try {
            $list = $this->query($sql);
            return $list[0];
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *更新条目
     **/
    public function editItem($moduleid, $modulename, $moduleurl, $syncfile, $isdelete)
    {
        $sql = "update sopr_module set modulename='%s', moduleurl='%s', syncfile='%s', isdelete=%s where moduleid=%s";
        $sql = sprintf($sql, $modulename, $moduleurl, $syncfile, $isdelete, $moduleid);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *删除条目
     **/
    public function delItem($moduleid)
    {
        $sql = "update sopr_module set isdelete=1 where moduleid=%s";
        $sql = sprintf($sql, $moduleid);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *删判断modulekey, 或者modulename是否已经存在
     **/
    public function isItemExist($modulekey, $modulename)
    {
        $sql = "select 1 from sopr_module where modulekey='%s' or modulename='%s'";
        $sql = sprintf($sql, $modulekey, $modulename);
        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }
}