<?php
namespace Admin\Model;

use \Common\Model\SoprModel;

class UserModel extends SoprModel
{
    protected $trueTableName = 'sopr_user';

    /**
     *新增条目
     **/
    public function addItem($username, $groupid, $isdelete)
    {
        $createtime = getCurrentDatetime();
        $sql = "insert into sopr_user values ('%s', %s, '%s', %s)";
        $sql = sprintf($sql, $username, $groupid, $createtime, $isdelete);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *新增条目:带权限的
     **/
    public function addItemTrans($username, $groupid, $isdelete, $moduleIdPermResultArr)
    {
        //生成user表插入语句
        $createtime = getCurrentDatetime();
        $userSql = "insert into sopr_user values ('%s', '%s', '%s', %s)";
        $userSql = sprintf($userSql, $username, $groupid, $createtime, $isdelete);

        //生成关联表插入语句
        $userModuleSql = "insert into sopr_user_module values ";
        $values = array();
        for ($i = 0, $len = count($moduleIdPermResultArr); $i < $len; $i++) {
            array_push($values, sprintf("('%s', %s, %s)", $username, $moduleIdPermResultArr[$i][0], $moduleIdPermResultArr[$i][1]));
        }
        $userModuleSql = $userModuleSql . join(",", $values);

        try {
            //事务开始
            $this->startTrans();
            if ($this->execute($userSql) === false) {
                $this->rollback();
                return false;
            }

            if ($this->execute($userModuleSql) === false) {
                $this->rollback();
                return false;
            }

            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->rollback();
            return false;
        }

    }

    /**
     *获取列表
     **/
    public function getItemList($username, $isdelete, $page)
    {
        $sql = "select username, groupid, (select groupname from sopr_group where groupid=sopr_user.groupid) as groupname, createtime, isdelete
        from sopr_user
        where username like '%%%s%%' and isdelete=%s limit %s, %s";
        $sql = sprintf($sql, $username, $isdelete, ($page - 1) * $this::PAGE_SIZE, $this::PAGE_SIZE);

        $totalnumSql = "select count(groupid) as totalnum
        from sopr_user
        where username like '%%%s%%' and isdelete=%s";
        $totalnumSql = sprintf($totalnumSql, $username, $isdelete);

        try {
            $list = $this->query($sql);
            $totalnum = $this->query($totalnumSql);
            $totalnum = $totalnum[0]["totalnum"];
            $totalpage = round($totalnum/$this::PAGE_SIZE);
            return array("list"=>$list, "totalnum"=>intval($totalnum[0]), "page"=>$page, "totalpage"=>$totalpage, pagesize=>$this::PAGE_SIZE);
        } catch (\Exception $e) {
            return false;
        }
    }


    /**
     *获取列表by groupid
     **/
    public function getItemListByGroupId($groupid)
    {
        $sql = "select username, groupid, createtime, isdelete from sopr_user where groupid=%d";
        $sql = sprintf($sql, $groupid);

        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *获取详情
     **/
    public function getItemDetail($username)
    {
        $sql = "select username, groupid, createtime, isdelete from sopr_user where username='%s'";
        $sql = sprintf($sql, $username);

        try {
            $list = $this->query($sql);
            return $list[0];
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *更新条目
     **/
    public function editItem($username, $groupid, $isdelete)
    {
        $sql = "update sopr_user set groupid='%s', isdelete=%s where username='%s'";
        $sql = sprintf($sql, $groupid, $isdelete, $username);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }


    /**
     *编辑条目:带权限的
     **/
    public function editItemTrans($username, $groupid, $isdelete, $moduleIdPermResultArr)
    {
        //生成user表插入语句
        $userSql = "update sopr_user set groupid='%s', isdelete=%s where username='%s'";
        $userSql = sprintf($userSql, $groupid, $isdelete, $username);

        //删除语句
        $delUserModuleSql = sprintf("delete from sopr_user_module where username='%s'", $username);

        //生成sopr_user_user插入语句
        $insertUserModuleSql = "insert into sopr_user_module values ";
        $values = array();
        for ($i = 0, $len = count($moduleIdPermResultArr); $i < $len; $i++) {
            array_push($values, sprintf("('%s', %s, %s)", $username, $moduleIdPermResultArr[$i][0], $moduleIdPermResultArr[$i][1]));
        }
        $insertUserModuleSql = $insertUserModuleSql . join(",", $values);

        try {
            //事务开始
            $this->startTrans();
            //先更新user表
            if ($this->execute($userSql) === false) {
                $this->rollback();
                return false;
            }

            //再删除旧关系数据
            if ($this->execute($delUserModuleSql) === false) {
                $this->rollback();
                return false;
            }
            //再插入新关系数据
            if ($this->execute($insertUserModuleSql) === false) {
                $this->rollback();
                return false;
            }

            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->rollback();
            return false;
        }

    }

    /**
     *删除条目
     **/
    public function delItem($username)
    {
        $sql = "update sopr_user set isdelete=1 where username='%s'";
        $sql = sprintf($sql, $username);
        try {
            return $this->execute($sql);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     *判断username是否已经存在
     **/
    public function isItemExist($username)
    {
        $sql = "select 1 from sopr_user where username='%s'";
        $sql = sprintf($sql, $username);
        try {
            return $this->query($sql);
        } catch (\Exception $e) {
            return false;
        }
    }
}