<?php
namespace Admin\Controller;

use \Common\Controller\SoprController;

class ModuleController extends SoprController{
    const MODULE_KEY = 'Module';
    public function index(){
        parent::checkReadPerm(false);
        $this->display();
    }

    public function addItem(){
        parent::checkWritePerm();
        //获取参数
        $modulekey = trim(I('modulekey', ""));
        $modulename = trim(I("modulename", ""));
        $moduleurl = trim(I("moduleurl", ""));
        $syncfile = trim(I("syncfile", ""));
        $isdelete = intval(trim(I("isdelete", 0)));

        if(!isNoEmptyString($modulekey)){
            $this->ajaxReturnError("modulekey字段非法");
        }
        if(!isNoEmptyString($modulename)){
            $this->ajaxReturnError("modulename字段非法");
        }
        if(!isString($moduleurl)){
            $this->ajaxReturnError("moduleurl字段非法");
        }
        if(!isString($syncfile)){
            $this->ajaxReturnError("syncfile字段非法");
        }
        if(!isdeleteInt($isdelete)){
            $this->ajaxReturnError("isdelete字段非法");
        }

        //获取模板对象
        $dao = new \Admin\Model\ModuleModel();
        //判断是否已经存在
        if(count($dao->isItemExist($modulekey, $modulename)) > 0){
            $this->ajaxReturnError("modulekey或者modulename已经存在");
        }
        //新增
        if($dao->addItem($modulekey, $modulename, $moduleurl, $syncfile, $isdelete)){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("新增失败");
        }
    }

    public function delItem(){
        parent::checkWritePerm();
        $moduleid = intval(trim(I("moduleid", "")));
        $dao = new \Admin\Model\ModuleModel();

        if(!isPositiveInt($moduleid)){
            $this->ajaxReturnError("moduleid字段非法");
        }

        $result = $dao->delItem($moduleid);
        if($result){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("删除失败");
        }
    }

    public function getItemList(){
        parent::checkReadPerm();
        $page = intval(trim(I("page", 1)));
        $modulekey = trim(I("modulekey", ""));
        $modulename = trim(I("modulename", ""));
        $isdelete = intval(trim(I("isdelete", 0)));

        if(!isPositiveInt($page)){
            $this->ajaxReturnError("page字段非法");
        }
        if(!isString($modulekey)){
            $this->ajaxReturnError("modulekey字段非法");
        }
        if(!isString($modulename)){
            $this->ajaxReturnError("modulename字段非法");
        }
        if(!isdeleteInt($isdelete)){
            $this->ajaxReturnError("isdelete字段非法");
        }

        $dao = new \Admin\Model\ModuleModel();
        $list = $dao->getItemList($modulekey, $modulename, $isdelete, $page);
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function getItemDetail(){
        parent::checkReadPerm();

        $moduleid = intval(trim(I("moduleid", "")));

        if(!isPositiveInt($moduleid)){
            $this->ajaxReturnError("moduleid字段非法");
        }
        $dao = new \Admin\Model\ModuleModel();
        $list = $dao->getItemDetail($moduleid);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function editItem(){
        parent::checkWritePerm();

        //获取参数
        $moduleid = intval(trim(I("moduleid", "")));
        $modulename = trim(I("modulename", ""));
        $moduleurl = trim(I("moduleurl", ""));
        $syncfile = trim(I("syncfile", ""));
        $isdelete = intval(trim(I("isdelete", 0)));

        if(!isPositiveInt($moduleid)){
            $this->ajaxReturnError("moduleid字段非法");
        }

        if(!isNoEmptyString($modulename)){
            $this->ajaxReturnError("modulename字段非法");
        }
        if(!isString($moduleurl)){
            $this->ajaxReturnError("moduleurl字段非法");
        }
        if(!isString($syncfile)){
            $this->ajaxReturnError("syncfile字段非法");
        }
        if(!isdeleteInt($isdelete)){
            $this->ajaxReturnError("isdelete字段非法");
        }

        $dao = new \Admin\Model\ModuleModel();
        $list = $dao->editItem($moduleid, $modulename, $moduleurl, $syncfile, $isdelete);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("编辑失败");
        }
    }

    public function getItemListByGroupId(){
        parent::checkReadPerm();

        $groupid = intval(trim(I("groupid", "")));
        if(!isPositiveInt($groupid)){
            $this->ajaxReturnError("groupid参数非法");
        }

        $dao = new \Admin\Model\ModuleModel();
        $list = $dao->getItemListByGroupId($groupid);
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function getItemListByUsername(){
        parent::checkReadPerm();

        $username = trim(I("username", ""));
        if(!isNoEmptyString($username)){
            $this->ajaxReturnError("username参数非法");
        }

        $dao = new \Admin\Model\ModuleModel();
        $list = $dao->getItemListByUsername($username);
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function getAllItemList(){
        parent::checkReadPerm();

        $dao = new \Admin\Model\ModuleModel();
        $list = $dao->getAllItemList();
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }
}