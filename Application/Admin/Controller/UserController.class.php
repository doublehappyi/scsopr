<?php
namespace Admin\Controller;

use \Common\Controller\SoprController;

class UserController extends SoprController{
    const MODULE_KEY = 'User';
    public function index(){
        parent::checkReadPerm(false);
        $this->display();
    }

    public function addItem(){
        parent::checkWritePerm();
        //获取参数
        $username = trim(I("username", ""));
        $groupid = intval(trim(I('groupid', "")));
        $isdelete = intval(trim(I("isdelete", 0)));
        //格式应该是这样的：moduleid1:perm1,moduleid2:perm2   例如：1:1,2:1,3:2
        $moduleidPerms = trim(I("moduleidPerms"), "");

        if(!isNoEmptyString($username)){
            $this->ajaxReturnError("username字段非法");
        }
        if(!isPositiveInt($groupid)){
            $this->ajaxReturnError("groupid字段非法");
        }
        if(!isdeleteInt($isdelete)){
            $this->ajaxReturnError("isdelete字段非法");
        }




        $moduleidPermResultArr = array();
        if(is_string($moduleidPerms)){
            $moduleidPermArr = explode(',', $moduleidPerms);

            //如果最后有一个逗号，则删除最后一个空元素.如果是$moduleidPerms是空字符串，则删除掉该数组中的唯一空元素
            if(trim($moduleidPermArr[count($moduleidPermArr) - 1]) == ''){
                array_pop($moduleidPermArr);
            }

            //如果中间任何一个元素是非法的，则提示错误
            for($i = 0, $len = count($moduleidPermArr); $i < $len; $i++){
                $currModuleIdPerm = $moduleidPermArr[$i];
                if(empty($currModuleIdPerm)){
                    $this->ajaxReturnError("moduleidPerms字段非法");
                    break;
                }
                $currModuleIdPermArr = explode(":",$currModuleIdPerm);
                if(count($currModuleIdPermArr) !== 2 || !isPositiveInt(intval(trim($currModuleIdPermArr[0]))) || !isPermInt(intval(trim($currModuleIdPermArr[1])))){
                    $this->ajaxReturnError("moduleidPerms字段非法");
                    break;
                }
                array_push($moduleidPermResultArr, $currModuleIdPermArr);
            }
        }else{
            $this->ajaxReturnError("moduleidPerms字段非法");
        }




        //获取模板对象
        $dao = new \Admin\Model\UserModel();
        //判断是否已经存在
        if(count($dao->isItemExist($username)) > 0){
            $this->ajaxReturnError("用户".$username."已经存在");
        }


        if(count($moduleidPermResultArr) > 0){
            //这里要对2个表进行操作，需要使用到事务
            if($dao->addItemTrans($username, $groupid, $isdelete, $moduleidPermResultArr)){
                $this->ajaxReturnSuccess();
            }else{
                $this->ajaxReturnError("新增失败");
            }
        }else{
            //新增
            if($dao->addItem($username, $groupid, $isdelete)){
                $this->ajaxReturnSuccess();
            }else{
                $this->ajaxReturnError("新增失败");
            }
        }

    }

    public function delItem(){
        parent::checkWritePerm();
        $username = trim(I("username", ""));
        $dao = new \Admin\Model\UserModel();

        if(!isNoEmptyString($username)){
            $this->ajaxReturnError("username字段非法");
        }

        $result = $dao->delItem($username);
        if($result){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("删除失败");
        }
    }

    public function getItemList(){
        parent::checkReadPerm();
        $page = intval(trim(I("page", 1)));
        $username = trim(I("username", ""));
        $isdelete = intval(trim(I("isdelete", 0)));

        if(!isPositiveInt($page)){
            $this->ajaxReturnError("page字段非法");
        }
        if(!isString($username)){
            $this->ajaxReturnError("username字段非法");
        }
        if(!isdeleteInt($isdelete)){
            $this->ajaxReturnError("isdelete字段非法");
        }

        $dao = new \Admin\Model\UserModel();
        $list = $dao->getItemList($username, $isdelete, $page);
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function getItemDetail(){
        parent::checkReadPerm();

        $username = trim(I("username", ""));

        if(!isNoEmptyString($username)){
            $this->ajaxReturnError("username字段非法");
        }
        $dao = new \Admin\Model\UserModel();
        $list = $dao->getItemDetail($username);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function editItem(){
        parent::checkWritePerm();

        //获取参数
        $username = trim(I("username", ""));
        $groupid = intval(trim(I('groupid', "")));
        $isdelete = intval(trim(I("isdelete", 0)));
        //格式应该是这样的：moduleid1:perm1,moduleid2:perm2   例如：1:1,2:1,3:2
        $moduleidPerms = trim(I("moduleidPerms"), "");

        if(!isNoEmptyString($username)){
            $this->ajaxReturnError("username字段非法");
        }
        if(!isPositiveInt($groupid)){
            $this->ajaxReturnError("groupid字段非法");
        }
        if(!isdeleteInt($isdelete)){
            $this->ajaxReturnError("isdelete字段非法");
        }


        $moduleidPermResultArr = array();
        if(is_string($moduleidPerms)){
            $moduleidPermArr = explode(',', $moduleidPerms);

            //如果最后有一个逗号，则删除最后一个空元素.如果是$moduleidPerms是空字符串，则删除掉该数组中的唯一空元素
            if(trim($moduleidPermArr[count($moduleidPermArr) - 1]) == ''){
                array_pop($moduleidPermArr);
            }

            //如果中间任何一个元素是非法的，则提示错误
            for($i = 0, $len = count($moduleidPermArr); $i < $len; $i++){
                $currModuleIdPerm = $moduleidPermArr[$i];
                if(empty($currModuleIdPerm)){
                    $this->ajaxReturnError("moduleidPerms字段非法");
                    break;
                }
                $currModuleIdPermArr = explode(":",$currModuleIdPerm);
                if(count($currModuleIdPermArr) !== 2 || !isPositiveInt(intval(trim($currModuleIdPermArr[0]))) || !isPermInt(intval(trim($currModuleIdPermArr[1])))){
                    $this->ajaxReturnError("moduleidPerms字段非法");
                    break;
                }
                array_push($moduleidPermResultArr, $currModuleIdPermArr);
            }
        }else{
            $this->ajaxReturnError("moduleidPerms字段非法");
        }




        //获取模板对象
        $dao = new \Admin\Model\UserModel();
        //判断是否已经存在
        if(count($dao->isItemExist($username)) == 0){
            $this->ajaxReturnError("用户".$username."不存在");
            return;
        }


        if(count($moduleidPermResultArr) > 0){
            //这里要对2个表进行操作，需要使用到事务
            if($dao->editItemTrans($username, $groupid, $isdelete, $moduleidPermResultArr)){
                $this->ajaxReturnSuccess();
            }else{
                $this->ajaxReturnError("新增失败");
            }
        }else{
            $list = $dao->editItem($username, $groupid, $isdelete);
            if($list !== false){
                $this->ajaxReturnSuccess($list);
            }else{
                $this->ajaxReturnError("编辑失败");
            }
        }
    }

    public function getItemListByGroupId(){
        parent::checkReadPerm();
        $groupid = intval(trim(I("groupid", "")));
        if(!isPositiveInt($groupid)){
            $this->ajaxReturnError("page字段非法");
        }

        $dao = new \Admin\Model\UserModel();
        $list = $dao->getItemListByGroupId($groupid);
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }
}