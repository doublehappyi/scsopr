<?php
namespace Admin\Controller;

use \Common\Controller\SoprController;

class GroupController extends SoprController{
    const MODULE_KEY = 'Group';
    public function index(){
        parent::checkReadPerm(false);
        $this->display();
    }
    
    public function addItem(){
        parent::checkWritePerm();
        //获取参数
        $groupname = trim(I("groupname", ""));
        $approver = trim(I('approver', ""));
        $isdelete = intval(trim(I("isdelete", 0)));
        //格式应该是这样的：moduleid1:perm1,moduleid2:perm2   例如：1:1,2:1,3:2
        $moduleidPerms = trim(I("moduleidPerms", ""));
        $usernames = trim(I("usernames", ""));


        if(!isNoEmptyString($groupname)){
            $this->ajaxReturnError("groupname字段非法");
        }
        if(!isdeleteInt($isdelete)){
            $this->ajaxReturnError("isdelete字段非法");
        }

        $moduleArr = array();
        if(is_string($moduleidPerms)){
            $moduleidPermArr = explode(',', $moduleidPerms);

            //如果最后有一个逗号，则删除最后一个空元素.如果是$moduleidPerms是空字符串，则删除掉该数组中的唯一空元素
            if(trim($moduleidPermArr[count($moduleidPermArr) - 1]) == ''){
                array_pop($moduleidPermArr);
            }

            //如果中间任何一个元素是非法的，则提示错误
            for($i = 0, $len = count($moduleidPermArr); $i < $len; $i++){
                $currModuleIdPerm = $moduleidPermArr[$i];
                $currModuleIdPermArr = explode(":",$currModuleIdPerm);
                if(count($currModuleIdPermArr) !== 2 || !isPositiveInt(intval(trim($currModuleIdPermArr[0]))) || !isPermInt(intval(trim($currModuleIdPermArr[1])))){
                    $this->ajaxReturnError("moduleidPerms字段非法");
                    break;
                }
                array_push($moduleArr, $currModuleIdPermArr);
            }
        }else{
            $this->ajaxReturnError("moduleidPerms字段非法");
        }

        if(is_string($usernames)){
            $usernameArr = explode(',', $usernames);
            //如果最后有一个逗号，则删除最后一个空元素.如果是$moduleidPerms是空字符串，则删除掉该数组中的唯一空元素
            if(trim($usernames[count($usernameArr) - 1]) == ''){
                array_pop($usernameArr);
            }

            //如果中间任何一个元素是非法的，则提示错误
            for($i = 0, $len = count($usernameArr); $i < $len; $i++){
                $currUsername = $usernameArr[$i];
                if(empty($currUsername)){
                    $this->ajaxReturnError("usernames字段非法");
                    break;
                }
            }
        }else{
            $this->ajaxReturnError("usernames字段非法");
        }

        //获取模板对象
        $dao = new \Admin\Model\GroupModel();
        //判断是否已经存在
        if(count($dao->isItemExist($groupname)) > 0){
            $this->ajaxReturnError("用户组已经存在");
        }

        //这里要对2个表进行操作，需要使用到事务
        $ret = $dao->addItemTrans($groupname, $approver, $isdelete, $moduleArr, $usernameArr);
        if($ret!== false){
            $this->ajaxReturnSuccess($ret);
        }else{
            $this->ajaxReturnError("新增失败");
        }
    }
    
    public function delItem(){
        parent::checkWritePerm();
        $groupid = intval(trim(I("groupid", "")));
        $dao = new \Admin\Model\GroupModel();

        if(!isPositiveInt($groupid)){
            $this->ajaxReturnError("groupid字段非法");
        }

        $result = $dao->delItem($groupid);
        if($result){
            $this->ajaxReturnSuccess();
        }else{
            $this->ajaxReturnError("删除失败");
        }
    }
    
    public function getItemList(){
        parent::checkReadPerm();
        $page = intval(trim(I("page", 1)));
        $groupname = trim(I("groupname", ""));
        $isdelete = intval(trim(I("isdelete", 0)));

        if(!isPositiveInt($page)){
            $this->ajaxReturnError("page字段非法");
        }
        if(!isString($groupname)){
            $this->ajaxReturnError("groupname字段非法");
        }
        if(!isdeleteInt($isdelete)){
            $this->ajaxReturnError("isdelete字段非法");
        }

        $dao = new \Admin\Model\GroupModel();
        $list = $dao->getItemList($groupname, $isdelete, $page);
        if($list!==false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

    public function getItemDetail(){
        parent::checkReadPerm();

        $groupid = intval(trim(I("groupid", "")));

        if(!isPositiveInt($groupid)){
            $this->ajaxReturnError("groupid字段非法");
        }
        $dao = new \Admin\Model\GroupModel();
        $list = $dao->getItemDetail($groupid);
        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }
    
    public function editItem(){
        parent::checkWritePerm();

        //获取参数
        $groupid = intval(trim(I("groupid", "")));
        $groupname = trim(I("groupname", ""));
        $approver = trim(I('approver', ""));
        $isdelete = intval(trim(I("isdelete", 0)));
        //格式应该是这样的：moduleid1:perm1,moduleid2:perm2   例如：1:1,2:1,3:2
        $moduleidPerms = trim(I("moduleidPerms", ""));
        //usernames格式是这样的：name1,name2,name3
        $usernames = trim(I("usernames", ""));

        if(!isPositiveInt($groupid)){
            $this->ajaxReturnError("groupid字段非法");
        }

        if(!isNoEmptyString($groupname)){
            $this->ajaxReturnError("groupname字段非法");
        }
        if(!isdeleteInt($isdelete)){
            $this->ajaxReturnError("isdelete字段非法");
        }

        $moduleArr = array();
        if(is_string($moduleidPerms)){
            $moduleidPermArr = explode(',', $moduleidPerms);

            //如果最后有一个逗号，则删除最后一个空元素.如果是$moduleidPerms是空字符串，则删除掉该数组中的唯一空元素
            if(trim($moduleidPermArr[count($moduleidPermArr) - 1]) == ''){
                array_pop($moduleidPermArr);
            }

            //如果中间任何一个元素是非法的，则提示错误
            for($i = 0, $len = count($moduleidPermArr); $i < $len; $i++){
                $currModuleIdPerm = $moduleidPermArr[$i];
                $currModuleIdPermArr = explode(":",$currModuleIdPerm);
                if(count($currModuleIdPermArr) !== 2 || !isPositiveInt(intval(trim($currModuleIdPermArr[0]))) || !isPermInt(intval(trim($currModuleIdPermArr[1])))){
                    $this->ajaxReturnError("moduleidPerms字段非法");
                    break;
                }
                array_push($moduleArr, $currModuleIdPermArr);
            }
        }else{
            $this->ajaxReturnError("moduleidPerms字段非法");
        }

        if(is_string($usernames)){
            $usernameArr = explode(',', $usernames);
            //如果最后有一个逗号，则删除最后一个空元素.如果是$moduleidPerms是空字符串，则删除掉该数组中的唯一空元素
            if(trim($usernames[count($usernameArr) - 1]) == ''){
                array_pop($usernameArr);
            }

            //如果中间任何一个元素是非法的，则提示错误
            for($i = 0, $len = count($usernameArr); $i < $len; $i++){
                $currUsername = $usernameArr[$i];
                if(empty($currUsername)){
                    $this->ajaxReturnError("usernames字段非法");
                    break;
                }
            }
        }else{
            $this->ajaxReturnError("usernames字段非法");
        }


        //获取模板对象
        $dao = new \Admin\Model\GroupModel();
//        //判断是否已经存在
//        if(count($dao->isItemExist($groupid)) === 0){
//            $this->ajaxReturnError("用户组不存在");
//            return;
//        }

        //这里要对2个表进行操作，需要使用到事务
        $ret = $dao->editItemTrans($groupid, $groupname, $approver, $isdelete, $moduleArr, $usernameArr);
        if($ret!== false){
            $this->ajaxReturnSuccess($ret);
        }else{
            $this->ajaxReturnError("新增失败");
        }
    }

    public function getAllItemList(){
        parent::checkReadPerm();
        $dao = new \Admin\Model\GroupModel();
        $list = $dao->getAllItemList();

        if($list !== false){
            $this->ajaxReturnSuccess($list);
        }else{
            $this->ajaxReturnError("获取失败");
        }
    }

}