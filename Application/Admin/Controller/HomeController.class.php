<?php
namespace Admin\Controller;

use \Common\Controller\SoprController;

class HomeController extends SoprController{
    public function index(){
        $username = $this->getUsernameFromCookie();
        $this->assign('username',$username);
        $this->display();
    }
}