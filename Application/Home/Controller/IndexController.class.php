<?php
namespace Home\Controller;
class IndexController extends \Common\Controller\SoprController{
    const ERP_LOGIN_DOMAIN = 'erp1.jd.com';
//    const ERP_LOGIN_URL = 'http://erp1.360buy.com/newHrm/Verify.aspx?ReturnUrl='; //即将过期
//    const ERP_LOGIN_KEY = '8B6697227CBCA902B1A0925D40FAA00B353F2DF4359D2099'; //测试环境的key
    const ERP_LOGIN_URL = 'http://ssa.jd.com/sso/login?ReturnUrl=';//已经开始使用
    const ERP_LOGIN_KEY = 'C602924B0D1090D931E3771D74ABBF9733A8C3545CFE1810'; //线上环境的key

    const MODULE_KEY = "Index";
    public function index(){
        parent::checkReadPerm();
        $username = $this->getUsernameFromCookie();
        $this->assign('username',$username);
        $this->display();
    }

    private function setCookieLogin($clientCookie, $key)
    {
        $client = new \Home\Common\JdUser ();
        $client->init_user($clientCookie, $key);
        $name = iconv("GB2312", "UTF-8", $client->username);
//        dump($name);
//        Yii::app()->session['clientname'] = $name;
    }


    public function login(){
        $ERP_LOGIN_COOKIE_NAME = C('ERP_LOGIN_COOKIE_NAME');
        $ERP_LOGIN_URL = C('ERP_LOGIN_URL');
        $ERP_LOGIN_KEY = C('ERP_LOGIN_KEY');

        if(cookie($ERP_LOGIN_COOKIE_NAME)){

        }else {
            $erpLoginUrl = $ERP_LOGIN_URL . "";
            redirect($erpLoginUrl);
        }
    }

    protected function getCurrUrl() {
        $sys_protocal = isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://';
        $php_self = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
        $path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
        $relate_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $php_self.(isset($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : $path_info);
        return $sys_protocal.(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '').$relate_url;
    }
}